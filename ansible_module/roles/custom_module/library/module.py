#!/usr/bin/python3
import psutil,json,socket
from ansible.module_utils.basic import AnsibleModule # lib for creating custom modules

def main():
    fields = {
        "hostname": {"default": True, "type": "bool"},
        "ip": {"default": True, "type": "bool"},
        "cpu": {"default": True, "type": "bool"},
        "mem": {"default": True, "type": "bool"},
        "swap": {"default": True, "type": "bool"},
        "disk": {"default": True, "type": "bool"},
        "network": {"default": True, "type": "bool"}
    }
    module = AnsibleModule(argument_spec=fields)  
    host_stat = {}
    if module.params['hostname']:
        host_stat['hostname'] = socket.gethostname() 
    if module.params['ip']:
        host_stat['ip'] = socket.gethostbyname(socket.gethostname())
    if module.params['cpu']:
        host_stat['cpu'] = {
            'cpu_count': psutil.cpu_count(),
            'load_in_perc_per_core': [str(x) + '%' for x in psutil.cpu_percent(interval=1,percpu=True)],
            'load_average': [str(x / psutil.cpu_count() * 100) + '%' for x in psutil.getloadavg()]
            }
    if module.params['mem']:
        mem = psutil.virtual_memory()
        host_stat['mem'] = {
            'total': mem.total / pow(1024,3),
            'total_avail': mem.available / pow(1024,3),
            'total_free': mem.free / pow(1024,3),
            'cached': mem.cached / pow(1024,3),
            'inactive': mem.inactive / pow(1024,3)
        }
    if module.params['swap']:
        swap = psutil.swap_memory()    
        host_stat['swap'] = {
            'swap': swap.total / pow(1024,3),
            'used': swap.used / pow(1024,3),
            'free': swap.free / pow(1024,3)
        }
    if module.params['disk']:
        disk = psutil.disk_io_counters(perdisk=True)
        host_stat['disk'] = {
            'read_count': disk
        }
    if module.params['network']:
        net = psutil.net_io_counters()
        net_card = psutil.net_if_stats()
        host_stat['network'] = {
            'bytes_sent': net.bytes_sent / pow(1024,3),
            'bytes_recv': net.bytes_recv / pow(1024,3),
            'net_card_stat': net_card
        }
    return_value = json.dumps(host_stat)
    module.exit_json(changed=False, meta=return_value)

if __name__ == '__main__':
    main()