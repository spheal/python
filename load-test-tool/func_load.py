import asyncio,os,time,requests # signle thread execution
# Make request and return the result
def fetch(url):
    started_at = time.monotonic()
    response = requests.get(url)
    request_time  = time.monotonic() - started_at
    return {"status_code": response.status_code, "request_time": request_time}

# Un/made request from a queue -> perform request -> return result to queue
async def worker(name,queue,results):
    loop = asyncio.get_event_loop()
    while True:
        url = await queue.get()
        if os.getenv("DEBUG"):
            print(f"{name} - Fetching {url}")
        future_result = loop.run_in_executor(None,fetch,url)
        result = await future_result
        results.append(result)
        queue.task_done()
# Divide work into batches and collect final result. This function will async
async def distr_work(url,requests,concurrency,results):
    # Create a queue that we will use to store "workers"
    queue = asyncio.Queue()
    for _ in range(requests):
        queue.put_nowait(url) # add X items at the end of the of the queue

    # Create X worker tasks to process the queue concurrently   
    tasks = []
    for i in range(concurrency):
        # Wrap the coroutine into a Task and schedule its execution
        task = asyncio.create_task(worker(f"worker-{i+1}", queue, results))
        tasks.append(task)
    
    started_at = time.monotonic()
    await queue.join() # block until all items in queue have been received and processed
    total_time = time.monotonic() - started_at

    for task in tasks:
        task.cancel()

    return total_time

# Entry point for setting params and starting processes
def entry_point(url,requests,concurrency):
    results = []
    total_time = asyncio.run(distr_work(url,requests,concurrency,results))
    return total_time,results