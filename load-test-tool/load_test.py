# Below code is based on LA course: https://linuxacademy.com/cp/modules/view/id/383

import click,json,sys
from func_load import entry_point
from stats import Results
@click.command() 
@click.option('--requests', '-r', default = 500, help = "Total number or requests")
@click.option('--concurrency', '-c', default = 1, help = "Number of concurent requests")
@click.option('--json-file', '-j', default = None, help = "Path to output result")
@click.argument('url')
def cli(requests, concurrency, json_file, url):
    if json_file:
        try:
            fd = open(json_file, "w")
        except:
            print(f"Unable to open {json_file}")
            sys.exit(1)
    total_time,request_dicts = entry_point(url,requests,concurrency)
    results = Results(total_time, request_dicts)
    display(results,fd)

def display(results, json_file):
    if json_file:
        json.dump(
            {
                "successful_requests": results.successful_requests(),
                "slowest": results.slowest(),
                "fastest": results.fastest(),
                "total_time": results.total_time,
                "rpm": results.rpm(),
                "rps": results.rps()
            },
            json_file,indent = 2
        )
        json_file.close()
        print("... Done")
    else:
        print("...Done\n--- Results ---")
        print(f"Successful results\t{results.successful_requests()}")
        print(f"Slowest result\t\t{results.slowest()}")
        print(f"Fastest result\t\t{results.fastest()}")
        print(f"Total time\t\t{results.total_time}")
        print(f"RPM\t\t\t{results.rpm()}")
        print(f"RPS\t\t\t{results.rps()}")

if __name__ == "__main__":
    cli()

