import click,json,sys

@click.command()
@click.option("--filename", "-f", default = None)
@click.option("--server", "-s", default = None, multiple = True)
def cli(filename, server):
    if not filename and not server:
        raise click.UsageError("Prvide servers or json file with list of servers to check")
    servers = set()
    if filename:
        try:
            with open(filename, "r") as fd:
                [servers.add(s) for s in json.load(fd)]
        except:
            print(f"Unable to open {filename}")
            sys.exit(1)
    
    if server:
        [servers.add(s) for s in server]

    print(servers)

if __name__ == "__main__":
    cli()