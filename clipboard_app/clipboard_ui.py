import tkinter as tk
import os
import platform
import pyperclip

# ClipBoardUI describes window with 15 latest values from clipboard
class ClipBoardUI(tk.Frame): # inherit tk.Frame class
    """
    Tkinter build a tree of widgets. Main window is root a other widgets connects to this root.
    """
    # Initialize all objects and set values
    def __init__(self, master=None):
        super().__init__(master) # inherit constructor from the parent class
        # Variables
        self.fields_number = 15 # number of fields to show clipboard values
        self.list_clipboard_values = list()
        # Methods
        self.pack() # make a window visible
        self.create_widgets() # initialize all elements
        self.tmp_file_init_read() # create tmp file for clipboard values
        self.update_clipboard_entries() # renew clipboard values


    # Define all window elements
    def create_widgets(self):
        self.fields_for_entries = [] # list of entries
        for field in range(self.fields_number): # Create 15 fields for clipboard entries
            self.clipboard_entry = tk.Entry(self, width=30)
            self.clipboard_entry.bind('<Double-Button-1>', self.highlight_copy_specified_value)
            self.clipboard_entry.grid(row=field, column=0) # set widget position
            self.fields_for_entries.append(self.clipboard_entry)
        # Create "quit" button
        self.quit_button = tk.Button(self, text="Quit", command=self.master.destroy)
        self.quit_button.grid(row=0, column=1)

    # Load previous clipboard entries
    def tmp_file_init_read(self):
        # Set temprorary dir based on Operating System.
        # For windows this value will be gatherd from 'TMP' environmnent variable.
        # For non-Windows it will be /tmp directory.
        tmp_dir = '/tmp/' if platform.system() != "Windows" else os.environ["TMP"] + "\\"
        self.tmp_clip_file = tmp_dir + "clipboard_data" # path to file with clipboard entries
        print("Path to the file with previous clipboard values: "+ self.tmp_clip_file)
        if os.path.exists(self.tmp_clip_file): # Read previous clipboard values
            # Read data from temp clipboard file
            with open(self.tmp_clip_file, "r") as tmp_file_clipboard:
                # Read content line by line
                self.list_clipboard_values = tmp_file_clipboard.read().splitlines()
            # Remove empty lines if they appeared in the file
            self.list_clipboard_values = list(filter(lambda clipboard_value: clipboard_value != "", self.list_clipboard_values))
            '''
            Problem 1
            In some cases temprorary file can contain more then 15 strings. That can cause "out of scope" list issue.
            Empty files causes the same issue.
            Need to fix this but currently have no idea how.
            '''
            if len(self.list_clipboard_values) < self.fields_number:
                # Write values from tmp_clip_file in entry fields
                for entry in range(len(self.list_clipboard_values)):
                    self.fields_for_entries[entry].insert(tk.END, self.list_clipboard_values[entry].strip())
            elif len(self.list_clipboard_values) > self.fields_number:
                for entry in range(self.fields_number):
                    self.fields_for_entries[entry].insert(tk.END, self.list_clipboard_values[entry].strip())


    # Get latest clipboard value and update clipboard entries on window
    def update_clipboard_entries(self):
        self.get_current_clipboard_entries(self.list_clipboard_values, self.tmp_clip_file)
        '''
        Related to problem 1
        '''
        if len(self.list_clipboard_values) < self.fields_number:
            # Write values from tmp_clip_file to entries
            for entry in range(len(self.list_clipboard_values)):
                self.fields_for_entries[entry].delete(0, tk.END)
                self.fields_for_entries[entry].insert(tk.END, self.list_clipboard_values[entry].strip())
        elif len(self.list_clipboard_values) > self.fields_number:
            for entry in range(self.fields_number):
                self.fields_for_entries[entry].delete(0, tk.END)
                self.fields_for_entries[entry].insert(tk.END, self.list_clipboard_values[entry].strip())
        # update entries every 3 seconds
        main_window.after(1000, self.update_clipboard_entries)

    # Gather latest clipboard values and write them to disk.
    def get_current_clipboard_entries(self, list_current_clipboard, tmp_clip_file):
        # get current clipboard value
        current_clipboard_value = pyperclip.paste()
        '''
        Related to problem 1
        '''
        if current_clipboard_value not in list_current_clipboard and len(list_current_clipboard) <= self.fields_number:
            list_current_clipboard.insert(0, current_clipboard_value)
        elif current_clipboard_value not in list_current_clipboard:
            list_current_clipboard.insert(0, current_clipboard_value)
            list_current_clipboard.pop()
        # write clipboard entries to file
        with open(tmp_clip_file, "w") as clip_file:
            # clip_file.write(clip_entry + "\n") for clip_entry in list_current_clipboard
            for clip_entry in list_current_clipboard:
                clip_file.write(clip_entry + "\n")

    # Highlight entire string when filed is selected
    @staticmethod
    def highlight_copy_specified_value(event):
        # highlight entire selected entry
        event.widget.select_range(0, tk.END)
        # copy to clipboard selected entry
        pyperclip.copy(event.widget.get())

# create blank window
main_window = tk.Tk()
# set name for the app
main_window.title("Clipboard")
# set window size
main_window.geometry("312x324")
# prevent window resizing
main_window.resizable(0, 0)
# initialize elements of the class on main window
app = ClipBoardUI(master=main_window)
# run application until closed
app.mainloop()
