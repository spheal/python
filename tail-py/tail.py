import sys,argparse,time,uuid,os,signal

def terminateProcess(signalNumber, frame): # Signal handler
    print("Terminating process")
    sys.exit(0)

def file_reader(file,option):
    try:
        with open(file, 'r') as fd:
            file_content = list(fd.read().splitlines())
            if option is None or len(file_content) < option:
                [print(line) for line in file_content]
            else:    
                result = file_content[len(file_content):len(file_content)-option-1:-1]
                [print(line) for line in result[::-1]]
    except OSError:
        print(f"Error: File {file} doesn't exist\n in the ")

# Implementation if -f flag from tail command
def file_follow(file):
    last_pos = 0
    try:
        with open(file) as fd:
            while True:
                fd.seek(last_pos) # set position to the end of file
                print(fd.read(),end = "") # read file from previous latest position
                last_pos = fd.tell() # get latest position on the file
                time.sleep(1)
    except OSError:
        print(f"Can't open {file}")
    

# def word_gen():
#     with open("test.txt",'a',buffering = 1) as file: # By default OS keeps data in buffer until certain value, by specifying buffering = 1 it will write data directly to the file
#         while True:
#             file.write(f"Random {uuid.uuid4()}\n")
#             time.sleep(0.2)


# CLI arguments
parser = argparse.ArgumentParser(description='Reading the file')
parser.add_argument('-n','--number' ,type=int, required = False,help='Number of lines to read')
parser.add_argument('-f','--follow',required=False,action='store_true',help = "Follow logs from the file") # action='store_true': default to False when the command-line argument is not present
parser.add_argument('file',nargs='+')
args = parser.parse_args()



if args.follow:
    signal.signal(signal.SIGINT, terminateProcess) # Handling SIGINT(ctr+c) signal
    file_follow(args.file[0]) # follow option will work only with one file
else:
    [ file_reader(file,args.number) for file in args.file ]


