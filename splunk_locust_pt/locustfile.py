import splunklib.client as client
from locust import HttpLocust, TaskSet, between
HOST='localhost'
PORT=8089
USERNAME='admin'
PASSWORD='password'

def login():
    service = client.connect(
    host=HOST,
    port=PORT,
    username=USERNAME,
    password=PASSWORD)
    return service

def logout(con):
    con.logout()

def monitoring_console(self):
    self.client.get("/app/splunk_monitoring_console/monitoringconsole_overview")

def health_check(self):
    self.client.get("/app/splunk_monitoring_console/monitoringconsole_check")

def job_summary(self):
    self.client.get("/app/launcher/job_manager?owner=")

def health_repot(self):
    self.client.get("/manager/system/health/manager")
    
def input_files(self):
    self.client.get("/manager/system/data/inputs/monitor")

def create_search(self):
    searchquery_normal = "search * | head 10"
    kwargs_normalsearch = {"exec_mode": "normal"}
    self.con.jobs.create(searchquery_normal, **kwargs_normalsearch)

def create_search_internal(self):
    searchquery_normal = "search index=_internal"
    kwargs_normalsearch = {"exec_mode": "normal"}
    self.con.jobs.create(searchquery_normal, **kwargs_normalsearch)

def create_search_audit(self):
    searchquery_normal = "search index=_audit"
    kwargs_normalsearch = {"exec_mode": "normal"}
    self.con.jobs.create(searchquery_normal, **kwargs_normalsearch)

def create_search_top_telem(self):
    searchquery_normal = 'search index="_telemetry"| top limit=20 "components{}.resultCount"'
    kwargs_normalsearch = {"exec_mode": "normal"}
    self.con.jobs.create(searchquery_normal, **kwargs_normalsearch)

class UserBehavior(TaskSet):
    tasks = {monitoring_console: 8,health_check: 10,job_summary: 10,health_repot: 10,input_files: 8,create_search: 1,create_search_internal: 1,create_search_audit: 1,create_search_top_telem: 1}
    def on_start(self):
        self.con = login()

    def on_stop(self):
        logout(self.con)



class WebsiteUser(HttpLocust):
    task_set = UserBehavior
    wait_time = between(0.100, 2.0)
    host = "http://localhost:8000"
